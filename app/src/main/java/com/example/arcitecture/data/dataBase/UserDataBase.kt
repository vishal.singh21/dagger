package com.example.arcitecture.data.dataBase

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [UserData1::class], version = 2)
abstract class UserDataBase : RoomDatabase() {

    abstract fun userDao() : UsersDao

    companion object {
        @Volatile
        private var INSTANCE : UserDataBase ?= null

        fun getDatabase(context: Context) : UserDataBase {
            if (INSTANCE == null) {
                synchronized(this) {}
                INSTANCE = Room.databaseBuilder(
                    context,
                    UserDataBase::class.java, "userDB"
                )
                    .build()
            }
            return INSTANCE!!
        }
    }


}