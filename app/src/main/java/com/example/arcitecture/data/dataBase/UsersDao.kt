package com.example.arcitecture.data.dataBase

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface UsersDao {

    @Insert
     fun insertUserDetail(userData1: UserData1)

    @Delete
    fun deleteUserDetail(userData1: UserData1)

    @Query("select * from userData1")
     fun getAllUser() : LiveData<List<UserData1>>

    @Update
    fun updateUser(userData1: UserData1)

}