package com.example.arcitecture.data.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.arcitecture.data.dataBase.UserDataBase
import com.example.arcitecture.data.dataBase.UserData1
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserRepository(
    private val userDataBase: UserDataBase,
    private val applicationContext : Context
) {

    private val userLiveData = MutableLiveData<List<UserData1>>()
    private val userLiveData2 = MutableLiveData<List<UserData1>>()

    val users : LiveData<List<UserData1>>
        get() = userDataBase.userDao().getAllUser()



    fun add(userData1: UserData1)
    {
        CoroutineScope(Dispatchers.IO).launch {
            val user = userDataBase.userDao().insertUserDetail(userData1 )
        }
    }
    fun updateUser(userData1: UserData1)
    {
        CoroutineScope(Dispatchers.IO).launch {
            userDataBase.userDao().updateUser(userData1)
        }
    }
    fun delete(userData1: UserData1)
    {
        CoroutineScope(Dispatchers.IO).launch {
            userDataBase.userDao().deleteUserDetail(userData1)
        }


    }
//    fun onlyFav()
//    {
//        CoroutineScope(Dispatchers.IO).launch {
//            userDataBase2
//        }
//    }
//    fun getAll()
//    {
//        CoroutineScope(Dispatchers.IO).launch {
//            val user = userDataBase.userDao().getAllUser()
//            userLiveData.postValue(user)
//            Log.e("db","${user}")
//        }
//
//    }

}